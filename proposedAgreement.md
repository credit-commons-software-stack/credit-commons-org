### Purpose
> **The Purpose of The Credit Commons Dev Collective is to develop and make available practical implementations of the ideas of the Credit Commons whitepaper, in the widest service of humanity.**

### Agreement: Members agree to;

1.	Act with other Members in good faith to further the Purpose of the Credit Commons Dev Collective.
2.	Join with other Members to form an unincorporated voluntary association ('The Association') without reference to the jurisdiction of any Nation State or Treaty Organisation.
3.	Abide by the Conditions of Membership, as follows:
    1.	Each Member desires to join The Association as an individual (organisations may be represented by individuals, but not be Members).
    2.	The existing Membership consents (in accordance with Policies as agreed by Members) to the Membership of each Member.
    3.	Membership will persist while both of these conditions are maintained, but not otherwise.
4. Act with other Members in good faith to democratically govern The Association in accordance with Policies as agreed by Members under the terms of this Agreement:
    1. All Members have equal voting rights - one member, one vote.
	3. Decisions on Policy issues are by simple majority on the basis of two-thirds participation unless otherwise specified.
	4. Decisions which would change, or go against the intent of, any part of the Founding Agreement require a 75% majority on the basis of 90% participation.
	5. Changes to the Purpose of The Association may not be made.
5. The work of the Members in respect of the Credit Commons will remain private to the Members of the Association until a decision is taken otherwise.
	1. Control over access to the work of the Members in respect of the Association will be maintained in the [Gitlab repo linked here](https://gitlab.com/credit-commons-software-stack/credit-commons-microservices) - or equivalent as selected by the Members.
	2. All Members will have 'Developer' access to the repo, and will choose amongst themselves who has 'Maintainer' permissions.
	3. Members will select three trusted individuals to act as 'Stewards' - to hold 'Owner' permissions over the repo, and whose responsibility it is to implement decisions of the Members in respect of access / publishing and the like.
	4. This Clause 5 will remain in force until July 4th 2020, at which point it will cease to have any effect - Members should decide upon appropriate Policy in respect of  access to the work at that point. It is agreed that this period is chosen to allow sufficient time for research into two issues: conditions around a/ raising of funds and  b/ appropriate licensing. The process and outcomes of that research will be considered as work of the Members under this clause.
	5. Use of the products of the work of Members should be made available to non-Members in any way that serves the Purpose, but should not go against the intent of this Clause.
	
### Policies
1. Membership is open to any individual meeting the Membership conditions.
4. All voting will take place on Loomio.
